import { SocketHub2Service } from './_lib/websocket/socket_hub2.service';

const www = new SocketHub2Service();

const hub = (data: any) => {
    if ( data.action === 'downstream' ) {
        return {
            to      : 'downstream',
            data    : data.data
        }
    }
    if ( data.action === 'mutual') {
        return {
            to      : 'mutual',
            data    : data.data
        }
    }
    if ( data.action === 'directionality') {
        return {
            to      : data.data.clientid,
            data    : data.data
        }
    }
}

// www.setNext(hub, 'room').start();
www.start();

