

export class DownStreamAction
{
    private static instance: DownStreamAction

    public static call(): DownStreamAction
    {
        if (!DownStreamAction.instance) {
            DownStreamAction.instance = new DownStreamAction();
        }
        return DownStreamAction.instance;
    }

    
}