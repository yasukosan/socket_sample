

export class MutualAction
{
    private static instance: MutualAction

    public static call(): MutualAction
    {
        if (!MutualAction.instance) {
            MutualAction.instance = new MutualAction();
        }
        return MutualAction.instance;
    }

    
}