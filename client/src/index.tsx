import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";

// import component
import SocketSample from './component/socketSample';
import HomeDownStream from './component/DownStream/HomeDownStream';
import HomeMutualStream from './component/MutualStream/HomeMutualStream';
import HomeDirectionality from './component/Directionality/HomeDirectionality';
import HomeRoom from './component/Room/HomeRoom';

require('./bootstrap');

render(
  <BrowserRouter>
    <Routes>
        <Route path="/" element={ <SocketSample /> }></Route>
        <Route path="/downstream" element={ <HomeDownStream page="" /> }></Route>
        <Route path="/mutualstream" element={ <HomeMutualStream page="" /> }></Route>
        <Route path="/directionality" element={ <HomeDirectionality /> }></Route>
        <Route path="/room" element={ <HomeRoom /> }></Route>
    </Routes>
  </BrowserRouter>
  , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

