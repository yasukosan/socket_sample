import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';
// import component

// import reducer
import {
    MutualStreamPropsInterface, initialState
} from '../../reducers/_Socket/MutualStream'


const MutualForm = ((state: MutualStreamPropsInterface) => {
    const dispatch = useDispatch();
    const d = useSelector((state: MutualStreamPropsInterface) => {
                    return (state.MutualStream) ? state.MutualStream : {
                        message : ['unko'],
                        job     : 'unko'
                    }
                });

    useWebsocketListner({
        next    : 'MutualAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Mutual Stream Test</h3>
            </nav>
            <br></br>

            <div>
                <input
                    id='MutualForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'SocketAction/Emit',
                                job     : 'cast',
                                data    : getMessage(),
                            });
                        }
                }>
                    送信
                </button>
            </div>
            <div>
                { showMessages(d.message) }
            </div>
            <div>
                接続中の全クライアントにメッセージを送信
            </div>
        </div>
    );
})

const getMessage = () => {
    const m = document.getElementById('MutualForm') as HTMLInputElement;
    return (m) ? m.value : '';
}

const showMessages = (messages : string[]) => {
    // console.log(messages);
    const _lists = Object.keys(messages).map((val, key) => {
        return (
            <div key={key}>
                {messages[key]}
            </div>
        );
    });
    return _lists;
                            
}

export default MutualForm;
