import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';
// import component

// import reducer
import {
    RoomPropsInterface
} from '../../reducers/_Socket/Room'

import {
    SocketPropsInterface, initialState as SocketInitial
} from '../../reducers/Socket';

const ChatRoom = (() => {
    const dispatch = useDispatch();
    const r = useSelector((state: RoomPropsInterface) => {
                    return (state.Room) ? state.Room : {
                        member   : [{user: 'unko', socket:'unko'}],
                        messages : [{user: 'unko', message:'unko'}],
                        room     : 'test'
                    }
                });
    
    const s = useSelector((state: SocketPropsInterface) => {
                    return (state.Socket) ? state.Socket : SocketInitial
                });

    useWebsocketListner({
        next    : 'RoomAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Room Strem Test</h3>
                <h4>MyID: {s.myId}</h4>
            </nav>
            <br></br>
            <div>
                Room：
                <input
                        id='Roomname'
                        type={'text'}
                    />
                <button
                        type='button'
                        className="btn btn-secondary btn-sm"
                        data-toggle="modal" data-target='#show_qr'
                        onClick={
                            () => {
                                dispatch({
                                    type    : 'RoomAction/emitHub',
                                    task    : 'join',
                                    room    : getRoomName(),
                                });
                            }
                    }>
                        接続
                </button>
            </div>
            <br></br>
            <div>
                Mess：<input
                    id='MessageForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'RoomAction/emitHub',
                                task    : 'message',
                                data    : {
                                    room    : r.room,
                                    message : getMessage(),
                                    myId    : s.myId
                                },
                            });
                        }
                }>
                    送信
                </button>
                <div className="user-list">
                    { showMember(r.member, dispatch) }
                </div>
            </div>
            <div>
                <table className='table'>
                    <tbody>
                    <tr>
                        <td>
                            <div className="message-list">
                                <ul className="list-group">
                                { showMessages(r.messages) }
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})

const getRoomName = () => {
    const u = document.getElementById('Roomname') as HTMLInputElement;
    return u.value;
}

const getMessage = () => {
    const m = document.getElementById('MessageForm') as HTMLInputElement;
    return m.value;
}

const setUserName = (name: string) => {
    const u = document.getElementById('Username') as HTMLInputElement;
    u.value = name;
}

const showMessages = (messages : object) => {
    const _lists = Object.keys(messages).map((val, key) => {
        if (messages[key]) {
            return (
                <li key={key} className="list-group-item message-content">
                    From : { ('name' in messages[key]) ? messages[key]['name'] : '' }<br/>
                    Mess : { ('message' in messages[key]) ? messages[key]['message'] : '' }
                </li>
            );
        }
    });
    return _lists;
}

const showMember = (users : object, dispatch: any) => {
    const _lists = Object.keys(users).map((val, key) => {
        return (
            <div key={key}
                onClick={(e: any) => {
                    setUserName(users[key]['socketid']);
                }}>
                Name: { users[key]['name'] }<br/>
                Sock: { users[key]['socketid'] }
            </div>
        );
    });
    return _lists;
}

export default ChatRoom;
