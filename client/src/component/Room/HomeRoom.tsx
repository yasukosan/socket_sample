import React from 'react';
import { Provider } from 'react-redux'
import { Link } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.css';

// import Component
import ChatRoom from './ChatRoom';

// import rootReducer from './reducers'
import { createStore } from '../../store/configureStore';

const store = createStore();

const HomeDirectionality = () => {
    return (
        <Provider store={ store }>
            <nav className="navbar navbar-dark bg-dark">
                <Link to="/" className="large_link navbar-brand">Home</Link>
            </nav>
            <div className="container-fluid">
                <ChatRoom />
            </div>
        </Provider>
    )
}

export default HomeDirectionality;