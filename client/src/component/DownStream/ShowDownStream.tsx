import React from 'react';
import { useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';
// import component

// import reducer
import {
    DownStreamPropsInterface
} from '../../reducers/_Socket/DownStream'


const ShowDownStream = (() => {
    const d = useSelector((state: DownStreamPropsInterface) => {
                    return (state.DownStream) ? state.DownStream : {
                        message : ['unko'],
                        job     : 'unko'
                    }
                });

    useWebsocketListner({
        next    : 'DownStreamAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Down Stream Test</h3>
            </nav>
            <br></br>
            <div>
                { showMessages(d.message) }
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})

const showMessages = (messages : string[]) => {
    console.log(messages);
    const _lists = Object.keys(messages).map((val, key) => {
        return (
            <div key={key}>
                {messages[key]}
            </div>
        );
    });
    return _lists;
                            
}

export default ShowDownStream;
