import React from 'react';
import { Provider } from 'react-redux'
import { Link } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.css';

import './_style/app.scss';
// import rootReducer from './reducers'
import { createStore } from '../store/configureStore';

const store = createStore();
interface AppPropsInterface {
  dispatch?: any;
}

export default class SocketSample
  extends React.Component <AppPropsInterface, {}>
{
  render() {
    return (
      <Provider store={ store }>
        <div>
            <h3>Socket Sample</h3>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="jumbotron"
                style={{width: '30rem', border: 'solid 1px gray', padding: '20px', margin: '10px', borderRadius: '10px'}}>
                <h1 className="display-4">DownStream</h1>
                <p className="lead">ダウンストリーム</p>
                <hr className="my-4" />
                <p>サーバーからの一方通行</p>
                <Link to="/downstream" className="btn btn-primary btn-lg">Start</Link>
              </div>
            </div>
            <div className="col">
              <div className="jumbotron"
                style={{width: '30rem', border: 'solid 1px gray', padding: '20px', margin: '10px', borderRadius: '10px'}}>
                <h1 className="display-4">Mutual</h1>
                <p className="lead">双方向通信</p>
                <hr className="my-4" />
                <p>全結合通信</p>
                <Link to="/mutualstream" className="btn btn-primary btn-lg">Start</Link>
              </div>
            </div>
            <div className="col">
              <div className="jumbotron"
                style={{width: '30rem', border: 'solid 1px gray', padding: '20px', margin: '10px', borderRadius: '10px'}}>
                <h1 className="display-4">directionality</h1>
                <p className="lead">ユーザー指定通信</p>
                <hr className="my-4" />
                <p>個別通信</p>
                <Link to="/directionality" className="btn btn-primary btn-lg">Start</Link>
              </div>
            </div>
            <div className="col">
              <div className="jumbotron"
                style={{width: '30rem', border: 'solid 1px gray', padding: '20px', margin: '10px', borderRadius: '10px'}}>
                <h1 className="display-4">Room</h1>
                <p className="lead">部屋通信</p>
                <hr className="my-4" />
                <p>部屋通信</p>
                <Link to="/room" className="btn btn-primary btn-lg">Start</Link>
              </div>
            </div>
          </div>
        </div>
      </Provider>
    )
  }
}
