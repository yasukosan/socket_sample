import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SocketClient2Service } from '../../_lib/websocket/socket_client2.service';
import { SocketInterface, SocketPropsInterface } from '../../reducers/Socket';

export interface WebSocket {
    next    : string,
}

export const useWebsocketListner = (state: WebSocket) => {
    const s = useSelector((state: SocketPropsInterface) => state.Socket);
    const dispatch = useDispatch();

    const socket = SocketClient2Service.call(8081, 'ws://localhost');

    useEffect(() => {
        socket
            // メッセージ受け取り処理
            .setNext((data: any) => {
                if (data.job === 'connection') {
                    dispatch({
                        type    : 'Socket/setMyId',
                        myId    : data.id,
                    });
                }
                console.log(data);
                dispatch({
                    type    : state.next,
                    data    : data,
                });
            }, 'message')
            .listen();
        
        // clientidを保存
    }, []);

    if (s && s.emit) {
        socket.send({
            job     : s.job,
            data    : s.message
        });
        dispatch({
            type    : 'Socket/clearEmit'
        });
    }

    return {message: ''};
};
