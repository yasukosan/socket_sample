import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useWebsocketListner } from '../_hook/websocket';

// import reducer
import {
    DirectionalityPropsInterface
} from '../../reducers/_Socket/Directionality'

import {
    SocketPropsInterface, initialState as SocketInitial
} from '../../reducers/Socket';


const Directionality = (() => {
    const dispatch = useDispatch();
    const d = useSelector((state: DirectionalityPropsInterface) => {
                    return (state.Directionality) ? state.Directionality : {
                        users   : [{user: 'unko', socket:'unko'}],
                        messages : [{user: 'unko', message:'unko'}],
                        job     : 'unko'
                    }
                });
    
    const s = useSelector((state: SocketPropsInterface) => {
                    return (state.Socket) ? state.Socket : SocketInitial
                });

    useWebsocketListner({
        next    : 'DirectionalityAction/onHub',
    });

    return (
        <div className="container">
            <nav className="navbar">
                <h3>Directionality Stream Test</h3>
                <h4>MyID: {s.myId}</h4>
            </nav>
            <br></br>
            <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'DirectionalityAction/emitHub',
                                task    : 'member',
                            });
                        }
                }>
                    接続(ユーザー一覧取得)
                </button>
            <div>
                <input
                    id='Username'
                    type={'text'}
                />
            </div>
            <div>
                <input
                    id='MessageForm'
                    type={'text'}
                />
                <button
                    type='button'
                    className="btn btn-secondary btn-sm"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={
                        () => {
                            dispatch({
                                type    : 'DirectionalityAction/emitHub',
                                task    : 'message',
                                data    : buildMessage(),
                            });
                        }
                }>
                    送信
                </button>
                <div className="user-list">
                    { showUsers(d.users, dispatch) }
                </div>
            </div>
            <div>
                <table className='table'>
                    <tbody>
                    <tr>
                        <td>
                            <div className="message-list">
                                <ul className="list-group">
                                { showMessages(d.messages) }
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div>
                localhost:8080/send にpostでデータを送信
            </div>
        </div>
    );
})

const getUserName = () => {
    const u = document.getElementById('Username') as HTMLInputElement;
    return u.value;
}

const setUserName = (name: string) => {
    const u = document.getElementById('Username') as HTMLInputElement;
    u.value = name;
}

const buildMessage = () => {
    const m = document.getElementById('MessageForm') as HTMLInputElement;
    const u = getUserName();
    return { message: m.value, user: u };
}

const showMessages = (messages : object) => {
    const _lists = Object.keys(messages).map((val, key) => {
        if (messages[key]) {
            return (
                <li key={key} className="list-group-item message-content">
                    From : { ('name' in messages[key]) ? messages[key]['name'] : '' }<br/>
                    Mess : { ('message' in messages[key]) ? messages[key]['message'] : '' }
                </li>
            );
        }
    });
    return _lists;
}

const showUsers = (users : object, dispatch: any) => {
    const _lists = Object.keys(users).map((val, key) => {
        return (
            <div key={key}
                onClick={(e: any) => {
                    setUserName(users[key]['socketid']);
                }}>
                Name: { users[key]['name'] }<br/>
                Sock: { users[key]['socketid'] }
            </div>
        );
    });
    return _lists;
}

export default Directionality;
