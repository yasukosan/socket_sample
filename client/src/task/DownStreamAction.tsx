import { put, takeEvery } from 'redux-saga/effects';

// import Helper

// import Reducer


// Root Saga登録配列
export const RootDownStreamAction = [
    takeEvery('DownStreamAction/onHub', onHub),
];


export function* onHub(val): any
{
    if ( 'job' in val.data && val.data.job === 'cast' ) {
        yield put({
            type    : 'DownStream/setMessage',
            message : toText(val.data.data, val.data.type)
        });
    }
    return;
}

const toText = (data: any, type: string): String => {
    if (type === 'String') {
        return data;
    }
    if (type === 'Object') {
        return JSON.stringify(data);
    }
    return '';
}
