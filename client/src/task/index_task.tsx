import { all } from 'redux-saga/effects';

// Load AnimationAction
import { AnimationTask } from '../animation/index.task';

import { RootTextAction } from './TextAction';
import { RootSocketAction } from './SocketAction';
import { RootDirectionalityAction } from './DirectionalityAction';
import { RootMutualAction } from './MutualAction';
import { RootDownStreamAction } from './DownStreamAction';
import { RootRoomAction } from './RoomAction';

export default function* rootSaga() {
    yield all([
        ...RootTextAction,
        ...RootSocketAction,
        ...RootDirectionalityAction,
        ...RootMutualAction,
        ...RootDownStreamAction,
        ...RootRoomAction,
        ...AnimationTask,
    ]);
}