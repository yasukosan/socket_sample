import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper


// import Reducer
import { ListTextPropsInterface, initialState as ltextInitial } from '../reducers/_Text/ListText';
import { initialState as orgTextInitial } from '../reducers/_Text/_text.Interface';

const listTextParam = (state: ListTextPropsInterface) => state.ListText;

// Root Saga登録配列
export const RootTextAction = [
    takeEvery('TextAction/add', add),
    takeEvery('TextAction/addImage', addImage),
];

export function* add(val: any): any
{
    console.log('add new text');
    const lt = yield select(listTextParam);
    const nt = Object.assign({}, orgTextInitial);
    nt.name = 'text';

    if (lt['texts'][0] === ltextInitial['texts'][0]) {
        yield put({
            type        : 'ListText/set',
            texts        : [nt],
        });
    } else {
        yield put({
            type        : 'ListText/add',
            text        : nt,
        });
    }
}

export function* addImage(val: any): any
{

}

