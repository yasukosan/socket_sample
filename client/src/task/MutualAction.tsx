import { put, takeEvery } from 'redux-saga/effects';

// import Helper

// import Reducer


// Root Saga登録配列
export const RootMutualAction = [
    takeEvery('MutualAction/onHub', onHub),
];


export function* onHub(val): any
{
    console.log(val);
    if ( 'job' in val.data && val.data.job === 'cast' ) {
        yield put({
            type    : 'MutualStream/setMessage',
            message : val.data.data
        });
    }
    return;
}


