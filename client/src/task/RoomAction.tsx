import { put, takeEvery } from 'redux-saga/effects';

// import Helper

// import Reducer


// Root Saga登録配列
export const RootRoomAction = [
    takeEvery('RoomAction/onHub', onHub),
    takeEvery('RoomAction/emitHub', emitHub),
];


export function* onHub(val): any
{
    if ( 'job' in val.data === false || val.data.job !== 'to_room' ) return;
    if ('job' in val.data.data === false) return;

    if (val.data.data.job === 'join') {
        yield put({
            type   : 'Room/setMember',
            member : val.data.data.message
        });
    }
    if (val.data.data.job === 'message') {
        yield put({
            type    : 'Room/setMessage',
            message : val.data.data.message
        });
    }

    return;
}

export function* emitHub(val): any
{
    console.log(val);
    if ( val.task === 'join' ) {
        yield put({
            type    : 'Room/setRoom',
            room    : val.room,
        });
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'join',
            data    : { room : val.room }
        });
    }
    if ( val.task === 'message' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'to_room',
            data    : {
                room    : val.data.room,
                message : {
                    message : val.data.message,
                    name    : val.data.myId,
                },
                job     : 'message'
            }
        });
    }
    return;
}