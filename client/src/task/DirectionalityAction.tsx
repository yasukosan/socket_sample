import { put, takeEvery } from 'redux-saga/effects';

// Root Saga登録配列
export const RootDirectionalityAction = [
    takeEvery('DirectionalityAction/onHub', onHub),
    takeEvery('DirectionalityAction/emitHub', emitHub),
    takeEvery('DirectionalityAction/getRoomMember', getRoomMember),
];


export function* onHub(val): any
{
    if ( 'job' in val.data && val.data.job === 'member' ) {
        yield put({
            type    : 'Directionality/setUser',
            users   : val.data.data
        });
    }
    if ( 'job' in val.data && val.data.job === 'to_client' ) {
        yield put({
            type    : 'Directionality/setMessage',
            message : {
                name    : 'user',
                message : val.data.data
            }
        });
    }
    return;
}

export function* emitHub(val): any
{
    console.log(val);
    if ( val.task === 'member' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'get_member',
            data    : { room : 'test' }
        });
    }
    if ( val.task === 'message' ) {
        yield put({
            type    : 'SocketAction/Emit',
            job     : 'to_client',
            data    : {
                client  : val.data.user,
                data    : val.data.message,
            }
        });
    }
    return;
}

export function* getRoomMember(): any
{

}

