import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper

// import Reducer
import {
    SocketPropsInterface, initialState
} from '../reducers/Socket';

const socketParam = (state: SocketPropsInterface) => state.Socket;

// Root Saga登録配列
export const RootSocketAction = [
    takeEvery('SocketAction/Emit', Emit),
    takeEvery('ScoketAction/addImage', addImage),
    takeEvery('ScoketAction/emitDownstream', emitDownstream),
    takeEvery('ScoketAction/emitMutual', emitMutual),
    takeEvery('ScoketAction/emitDirectionality', emitDirectionality),

    takeEvery('ScoketAction/getMember', getMember),
    takeEvery('ScoketAction/getRoomMember', getRoomMember),
];

export function* Emit(val: any): any
{
    console.log('Emit Message');
    const lt = yield select(socketParam);
    
    yield put({
        type        : 'Socket/setEmit',
        job         : val.job,
        message     : val.data,
    });
}

export function* addImage(val: any): any
{

}

export function* emitDownstream(val: any): any
{
    if ('data' in val && val.data.type === 'String') {
        yield put({
            type    : 'DownStream/setMessage',
            message : val.data.data,
        });
    }
}

export function* emitMutual(val: any): any
{
    if ('data' in val && val.data.type === 'String') {
        yield put({
            type    : 'MutualStream/setMessage',
            message : val.data.message,
        });
    }
}

export function* emitDirectionality(val: any): any
{
    if (val.message.type === 'Object' && val.message.message !== {}) {
        console.log(`
        [EmitDirectionalityMessage]
        ${JSON.stringify(val)}
        `);
        
        yield put({
            type    : 'Directionality/setMessage',
            user    : {
                user    : val.message.data.user,
                socket  : val.message.data.socket,
            },
        });
        yield put({
            type    : 'Directionality/setMessage',
            message : {
                user    : val.message.data.user,
                message : val.message.data.form
            },
        });
    }
}

export function* getMember(val): any
{
    yield put({
        type    : 'SocketAction/Emit',
        job     : val.job,
        data    : val.data
    });
}

export function* getRoomMember(): any
{

}

