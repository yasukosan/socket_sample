
import { animationReducers } from '../animation/index.reducer';
import { SocketReducer } from './_Socket/index.reducers';
import Server from './Server';
import Socket from './Socket';

export const reducer = {
    ...animationReducers,
    ...SocketReducer,
    Socket,
    Server,
}



