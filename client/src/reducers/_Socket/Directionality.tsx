import { createSlice } from '@reduxjs/toolkit';

export interface DirectionalityPropsInterface
{
    Directionality? : DirectionalityInterface,
    dispatch?       : any;
}

export interface DirectionalityInterface
{
    job         : string,
    users        : [{
        name        : string,
        socketid    : string,
    }],
    messages     : [{
        name    : string,
        message : string,
    }],
}

const initialUser = {
    name        : 'unko',
    socketid    : 'unko',
}

const initialMessage = {
    name    : 'unko',
    message : 'unko',
}


export const initialState: DirectionalityInterface = {
    job         : 'send',
    users       : [initialUser],
    messages    : [initialMessage],
}



const slice = createSlice({
    name: 'Directionality',
    initialState,
    reducers: {
        setUser: (state: any, action: any) => {
            if (action.users === undefined) return state;
            return Object.assign({}, state,
                {
                    users : action.users,
                }
            )
        },
        addUser: (state: any, action: any) => {
            if (action.user === undefined) return state;
            const u = state.users;
            const _u = (u === initialUser)
                            ? [action.user] : u.concat([action.user]);
            if (u in action.user === false) {
                return Object.assign({}, state,
                    {
                        users : _u,
                    }
                )
            }
        },
        setMessage: (state: any, action: any) => {
            if (action.message === undefined
                || action.message.name === undefined) return state;
            console.log(action.message);
            const m = state.messages;
            return Object.assign({}, state,
                {
                    messages : m.concat([action.message]),
                }
            )
        },
        setjob: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    job : action.job,
                }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
