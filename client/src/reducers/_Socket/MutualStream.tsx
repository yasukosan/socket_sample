import { createSlice } from '@reduxjs/toolkit';

export interface MutualStreamPropsInterface
{
    MutualStream?   : MutualStreamInterface,
    dispatch?       : any;
}

export interface MutualStreamInterface
{
    job         : string,
    message     : string[],
}

export const initialState: MutualStreamInterface = {
    job         : 'send',
    message     : [''],
}

const slice = createSlice({
    name: 'MutualStream',
    initialState,
    reducers: {
        setMessage: (state: any, action: any) => {
            const m = state.message;
            return Object.assign({}, state,
                {
                    message : m.concat([action.message]),
                }
            )
        },
        setjob: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    job : action.job,
                }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
