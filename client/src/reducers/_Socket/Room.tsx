import { createSlice } from '@reduxjs/toolkit';

export interface RoomPropsInterface
{
    Room? : RoomInterface,
    dispatch?       : any;
}

export interface RoomInterface
{
    room        : string,
    messages    : [{
        name    : string,
        message : string,
    }],
    member      : [{
        name    : string,
        socket  : string,
    }]
}

const initialMessage = {
    name    : 'unko',
    message : 'unko',
}
const initialMember = {
    name    : 'unko',
    socket  : 'unko',
}

export const initialState: RoomInterface = {
    room        : 'test',
    messages    : [initialMessage],
    member      : [initialMember],
}



const slice = createSlice({
    name: 'Room',
    initialState,
    reducers: {
        setMessage: (state: any, action: any) => {
            if (action.message === undefined
                || action.message.name === undefined) return state;
            console.log(action.message);
            const m = state.messages;
            return Object.assign({}, state,
                {
                    messages : m.concat([action.message]),
                }
            )
        },
        setMember: (state: any, action: any) => {
            if (action.member === undefined
                || action.member[0].name === undefined) return state;
            console.log(action.member);
            const m = state.member;
            return Object.assign({}, state,
                {
                    member : action.member,
                }
            )
        },
        setRoom: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    room : action.room,
                }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
