import DownStream from './DownStream';
import MutualStream from './MutualStream';
import Directionality from './Directionality';
import Room from './Room';

export const SocketReducer = {
    DownStream      : DownStream,
    MutualStream    : MutualStream,
    Directionality  : Directionality,
    Room            : Room
}
