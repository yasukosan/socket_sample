import { createSlice } from '@reduxjs/toolkit';

export interface SocketPropsInterface
{
    Socket?     : SocketInterface,
    dispatch?   : any;
}

export interface SocketInterface
{
    emit        : boolean,
    job         : string,
    message     : any,
    myId        : string,
}

export const initialState: SocketInterface = {
    emit        : false,
    job         : 'send',
    message     : {},
    myId        : '',
}

const slice = createSlice({
    name: 'Socket',
    initialState,
    reducers: {
        setEmit: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    emit    : true,
                    job     : action.job,
                    message : action.message,
                }
            )
        },
        clearEmit: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    emit    : initialState.emit,
                    job     : initialState.job,
                    message : initialState.message,
                }
            )
        },
        setMyId: (state: any, action: any) => {
            return Object.assign({}, state,
                {
                    myId    : action.myId,
                }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
